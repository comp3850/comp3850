1. Rename "Actual Codes (to be renamed) when possible. (Maybe call it DVT or TheDVT?)
   (Completed in Commit 54ef319)
2. Add working info table on right side of network graph
   (Working example so far, might need to be cleaned up)
3. Add filter panel on the left of the network graph, think nodes and relationships specific to the database (clients mention of filtering what nodes you're seeing)
   (Dynamically creates filter table based on database architecture. Not fully functional)
4. Different view below (table, bar, pie) maybe showing holistic data view, as network graph isn't very good at all data together at once.

5. Simple search bar of network graph
   (Think this is working pretty well now :) should try and fix the security of it because it's very vulnerable to injection attacks)

6. Combining two nodes information, see specific relationships or how many steps it takes to connect them.
   

* ***Last*** Make Look Pretty :D
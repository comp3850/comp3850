import PropTypes from 'prop-types';

async function loginUser(credentials) {
  return fetch('http://localhost:8080/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
  .then(data => data.json())
  .catch(err => {
    console.log(err);
  });
}

const Login = ( {setToken} ) => { 

  const handleSubmit = async (event) => {
    event.preventDefault();
    const user = event.target[0].value;
    const pass = event.target[1].value;
    const credentials = {user, pass};
    const token = await loginUser(credentials);
    if(token.token !== 'devTeamAdmin') {
      document.getElementById('wrongPassword').style.display = "block"
    } else {
      setToken(token);
    }
  }

  return <>
    <div className="div-container-LoginPage">
      <h1><u>Login to Continue</u></h1>
      <form onSubmit={handleSubmit} className={"login-form"}>
        <label>
          <input type="text" name="username" placeholder={"Username"}/>
        </label>
        <label>
          <input type="password" name="password" placeholder={"Password"}/>
        </label>
        <div>
          <button type="submit" value={"Submit"}><strong>Submit</strong></button>
        </div>
        <div style={{display: "none"}} id={"wrongPassword"}><strong>Incorrect Login Credentials: Try Again Please</strong></div>
      </form>
    </div>
  </>
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
};

export default Login;
import neo4j from 'neo4j-driver'
import ForceGraph3D from '3d-force-graph'
import React from 'react';


const url = 'neo4j+s://f8bffb42.databases.neo4j.io';
const user = 'neo4j';
const password = 'mental-society-rondo-sleep-tornado-8822';

const driver = neo4j.driver(url, neo4j.auth.basic(user, password));

const TreeDGraph = () => {

  const content = (
    <>
      <div id='3d-graph'></div>
      <button onClick={makeGraph}>Draw 3D Graph</button>
    </>
  )

  return content;
}

const makeGraph = async () => {
  const session = driver.session();

  try {
    const result = await session.run("MATCH (p)-->(m) RETURN id(p) as source, id(m) as target, p,m");
    console.log(result);
    const links = result.records.map(r => {
      return {
        source: r.get('source').toNumber(),
        target: r.get('target').toNumber()
      }
    });
    const ids = new Set();
    links.forEach(link => {
      ids.add(link.source);
      ids.add(link.target);
    });
    const formattedData = { nodes: Array.from(ids).map(id => { return { id } }), links: links }
    drawGraph(formattedData);
    return console.log(formattedData);
  } catch (e) {
    console.log(e);
  } finally {
    session.close();
  }
}

const drawGraph = (data) => {
  const graph = new ForceGraph3D()
    (document.getElementById('3d-graph'))
    .nodeAutoColorBy('group')
    .nodeVal(5)
    .onNodeClick(() => {
      console.log("node Clicked!")
    })
    .linkWidth(3)
    .width(1500)
    .height(800)
    .graphData(data);
  console.log(graph)
}

export { TreeDGraph }
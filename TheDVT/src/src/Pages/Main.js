import {GraphCanvas} from "../Components/GraphCanvas";
import {TableCanvas} from "../Components/TableCanvas";

const main = () => { 
  
  return <>
    <div className="div-container-main">
      <div className="main-nav">
        <h1><u>TheDVT</u></h1>
        <button onClick={handleOnClickLogout}>Logout</button>
      </div>
      <GraphCanvas />
      <TableCanvas />
    </div>
  </>
}

const handleOnClickLogout = async (event) => {
  event.preventDefault();
  localStorage.removeItem("token");
  window.location.reload();
}

export default main;
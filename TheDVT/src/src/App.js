//REACT ROUTES IMPORT
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';

//COMPONENTS & LOGIC
import useToken from './Components/useToken';

//VIEWS AND PAGES
import Login from './Pages/Login';
import Main from './Pages/Main';
import Preferences from './Pages/Preferences';
import { TreeDGraph } from './Pages/TreeDGraph';

function App() {

  //Token set state
  const { token, setToken } = useToken();

  //Token authentication done in backend
  if(!token) {
    return <Login setToken={setToken}/>
  }

  return (
    <div className='app'>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Main/>}/>
          <Route path='/preferences' element={<Preferences/>}/>
          <Route path='/TreeDGraph' element={<TreeDGraph/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

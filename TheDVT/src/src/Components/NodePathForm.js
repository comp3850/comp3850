import { getNodeRecIdentities } from "./DatabaseInfo"; 
import { pathBetweenHandle } from "./GraphCanvas";

const NodePathForm = () => {

  const content = (
    <>
    
      <form className={"TwoPathForm"} onSubmit={pathBetweenHandle}>
        <div>
          <p>NODE PATH CONNECTION</p>
        </div>
        <div>
          <label>First Node</label>
          <select id={"FirstNodePathInput"} className={"selectForm"}>
            <option>Choose</option>
        </select>
        </div>
        <div>
          <label>Second Node</label>
          <select id={"SecondNodePathInput"} className={"selectForm"}>
            <option>Choose</option>
          </select>
        </div>
        <button type={"submit"}>Find Connection</button>
      </form>
    </>
  );

  return content;
}

const populateFormOptions = async () => {
  const firstInput = document.getElementById("FirstNodePathInput");
  const secondInput = document.getElementById("SecondNodePathInput");
  const popData = await getNodeRecIdentities();
  
  popData.forEach(data => {
    firstInput.options.add(new Option(data, data));
    secondInput.options.add(new Option(data, data));
  })
}

export { NodePathForm, populateFormOptions }
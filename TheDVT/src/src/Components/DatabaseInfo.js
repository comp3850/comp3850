import neo4j from 'neo4j-driver'

const url = 'bolt://c2d03732319c6cad7fe1af6bc085e911.neo4jsandbox.com:7687'//'neo4j+s://f8bffb42.databases.neo4j.io';
const user = 'neo4j';
const password = 'stoppers-transfers-drill'//'mental-society-rondo-sleep-tornado-8822';

const driver = neo4j.driver(url, neo4j.auth.basic(user,password));

//DEFAULT QUERIES
//const schemaQuery = 'call db.schema.visualization';
const listNodeQuery = 'call db.labels()';
const listRelationshipsQuery = 'call db.relationshipTypes()';
const countRelsQuery = 'MATCH (p)-[r]->(m) RETURN type(r),count(r)';
const countNodesQuery = 'MATCH (p) RETURN labels(p), count(p)';

// Custom Queries That Will Only Work When Configured Against Specific Database Connected
const countOfRelsPerNodeQuery = 'MATCH (n:Client) RETURN n AS NODE, size((n)-->()) AS COUNT';
const getAllNodeTitles = 'MATCH (n) WHERE EXISTS (n.title) RETURN n.title';
const getAllNodeNames = 'MATCH (n) WHERE EXISTS (n.name) RETURN n.name';

const getNodeNames = async () => {
  const session = driver.session();

  try{
    const readResult = await session.run(listNodeQuery);
    const arrayOfNodeNames = [];
    readResult.records.forEach(record => {
      arrayOfNodeNames.push(record._fields[0]);
    })
    console.log('Sent to database to get node Names: ' + arrayOfNodeNames);
    return arrayOfNodeNames
  } catch (error) {
    console.log("This is a problem :/ : " + error);
  } finally {
    await session.close();
  }
}

  const getRelsNames = async () => {
    const session = driver.session();

    try{
      const readResult = await session.run(listRelationshipsQuery);
      const arrayOfRelsNames = [];
      readResult.records.forEach(record => {
        arrayOfRelsNames.push(record._fields[0]);
      })
      console.log('Sent to database to get relationship Names: ' + arrayOfRelsNames);
      return arrayOfRelsNames
    } catch (error) {
      console.log("This is a problem :/ : " + error);
    } finally {
      await session.close();
    }
  }

  const countRels = async () => {
    const session = driver.session();

    try {
      const readResult = await session.run(countRelsQuery);
      const resultArray = [];
      readResult.records.forEach(record => {
        let rel = record._fields[0]
        let count = record._fields[1].low
        let parcel = {rel, count}
        resultArray.push(parcel)
      })
      return resultArray;
    } catch (error) {
      console.log("This is a problem :/ : " + error);
    } finally {
      await session.close();
    }
  }

  const countNodes = async () => {
    const session = driver.session();

    try {
      const readResult = await session.run(countNodesQuery);
      const resultArray = [];
      readResult.records.forEach(record => {
        let node = record._fields[0]
        let count = record._fields[1].low
        let parcel = {node, count}
        resultArray.push(parcel)
      })
      return resultArray;
    } catch (error) {
      console.log("This is a problem :/ : " + error);
    } finally {
      await session.close();
    }
  }

  const countOfRelsPerNode = async () => {
    const session = driver.session();

    try {
      const readResult = await session.run(countOfRelsPerNodeQuery);
      const resultArray = [];
      for(let i=0;i<10;i++) {
        let node = readResult.records[i]._fields[0].properties.name
        let count = readResult.records[i]._fields[1].low
        let parcel = {node, count}
        resultArray.push(parcel)
      }
      return resultArray;
    } catch (error) {
      console.log("This is a problem :/ : " + error);
    } finally {
      await session.close();
    }
  }

  const getNodeRecIdentities = async () => {
    const session = driver.session();

    try {
      const resultArray = [];
      const titleResults = await session.run(getAllNodeTitles);
      const nameResults = await session.run(getAllNodeNames);
      titleResults.records.forEach(title => {
        resultArray.push(title._fields[0]);
      })
      nameResults.records.forEach(names => {
        resultArray.push(names._fields[0]);
      })
      console.log(titleResults);
      resultArray.sort();
      return resultArray;
    } catch (error) {
      console.log("This is a problem :/ : " + error);
    } finally {
      await session.close();
    }
  }



export { getNodeNames, getRelsNames, countRels, countNodes, countOfRelsPerNode, getNodeRecIdentities, url, user, password }
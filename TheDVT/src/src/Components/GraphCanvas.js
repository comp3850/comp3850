import NeoVis from 'neovis.js'

import { NodePathForm, populateFormOptions } from './NodePathForm'
import { getNodeNames, getRelsNames, url, user, password } from './DatabaseInfo'

const config = {
  container_id: "viz",
  server_url: url,
  server_user: user,
  server_password: password,
  labels: {
    "Client": {
      "caption": 'name',
    },
    "Mule": {
      "caption": "name",
    },
    "Merchant": {
      "caption": "name",
    },
    "Bank": {
      "caption": "name",
    },
    "CashIn": {
      "size": "amount",
    },
    "CashOut": {
      "size": "amount",
    },
    "Payment": {
      "size": "amount",
    },
    "Transfer": {
      "size": "amount",
      "community": "community",
    },
    "Email": {
      "caption": "email"
    }
  },
  encrypted: "ENCRYPTION_ON"
}

const GraphCanvas = () => {
  
  return(
    <>
      <div className={"div-container-GraphCanvas"}>
        <div className={"start-container"}>
          {/* <p>CLICK BELOW TO START</p> */}
          <button className={"GraphCanvas-button"} onClick={() => {functionHandler(); visualiseData(); populateFormOptions(); checkMouseOver();}}><u>CLICK TO START</u></button>
          <button className={"GraphCanvas-clear"} onClick={clearVis}>CLEAR VISUALS</button>
        </div>
        <div className={"div-container-search"}>
          <form className={"SimpleSearchForm"} onSubmit={searchHandle}>
            <label>SEARCH</label>
            <textarea type={"text"} name={"query"} rows={"4"} placeholder={"Type here to search the database..."}/>
            <button onClick={() => {checkMouseOver(); functionHandler(); populateFormOptions();}} type={"submit"}>Search</button>
          </form>
          <div className={"div-container-Connections"}>
            <NodePathForm/>
          </div>
          <form className={"GraphQueryForm"} onSubmit={customQueryVis}>
            <label>QUERY</label>
            <textarea type={"text"} name={"query"} rows={"4"} placeholder={"Write a query here to search database..."}/>
            <button onClick={() => {checkMouseOver(); functionHandler(); populateFormOptions();}} type={"submit"}>Visualise</button>
          </form>
        </div>
        
        
        {/* TAKE OUT LATER */}
        {/* <button onClick={populateFormOptions}>TestFunctionButton</button>  */}
        
        <div className={"div-container-GraphCanvas-content"}>
          <div id='viz' style={{height: '650px', borderStyle: 'solid', margin: '1em', background: 'lightgray'}}></div>
          <div className={'filterBoxForm'}>
            <form onSubmit={handleFilterSubmit}>
              <table id={"filterBox-table"} className={"filterBox-table"}>
                <tbody>
                  <tr>
                    <th><strong>Filter</strong></th>
                  </tr>
                </tbody>
              </table>
            </form>
          </div>
          <div className={"PanelInfo"}>
            <table id={"GraphDataInfoTable"} className={"GraphDataInfoTable"}>
              <tbody>
                <tr>
                  <th>Node and Relationship Data</th>
                </tr>
                <tr id={"dataInfo"}>

                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}

const functionHandler = () => {
  if(document.getElementById('filterBox-table').firstChild.childElementCount <=1) {
    addToFilterTable()
  };
}

const visualiseData = () => {
  config.initial_cypher = "MATCH (p)-[r]->(m) RETURN p,r,m LIMIT 500"
  let graph = new NeoVis(config);
  console.log(graph);
  graph.render();
}

const clearVis = () => {
  config.initial_cypher = "";
  let graph = new NeoVis(config);
  console.log(graph)
  graph.render();
}

const customQueryVis = async (event) => {
  event.preventDefault();
  
  const query = event.target[0].value;
  console.log(query)
  config.initial_cypher = query;

  try {
    let graph = new NeoVis(config);
    console.log(graph)
    graph.render();
  } catch (err) {
    console.log(err);
  }
}

const checkMouseOver = () => {
  const canvas = document.getElementById('viz');

  if(canvas) {
    canvas.addEventListener('mousedown', () => {
      if(getStyle() === 'visible') {
        addDataToTable();
      }
    })
  }
}

const getStyle = () => {
  try {
    const element = document.querySelector(".vis-tooltip");
    const elementStyles = getComputedStyle(element);
    return elementStyles.visibility;
  } catch (err) {
    console.log(err);
  }
}

const addDataToTable = () => {
  const tableRow = document.getElementById('dataInfo');
  const visToolTip = document.querySelector('.vis-tooltip');
  if(visToolTip) {
    tableRow.innerHTML = "";
    tableRow.insertCell(0).innerHTML = visToolTip.innerHTML;
    return visToolTip.innerHTML;
  }
}

const addToFilterTable = async () => {
  const filterTable = document.getElementById('filterBox-table');
  
  const nodeNames = await getNodeNames();
  console.log(nodeNames);
  filterTable.insertRow(-1).insertCell(0).innerHTML = "<strong>NODES - </strong>";
  nodeNames.forEach((nodes) => {
    filterTable.insertRow(-1).insertCell(0).innerHTML = '<label for='+nodes+'>'+nodes+': </label><input type="radio" id="'+nodes+'" name="myCheckbox"></input>';
  })
  
  const relNames = await getRelsNames();
  console.log(relNames);
  filterTable.insertRow(-1).insertCell(0).innerHTML = "<strong>RELATIONSHIPS - </strong>";
  relNames.forEach((rels) => {
    filterTable.insertRow(-1).insertCell(0).innerHTML = '<label for='+rels+'>'+rels+': </label><input type="radio" id="'+rels+'" name="myCheckbox"></input>';
  })

  filterTable.insertRow(-1).insertCell(0).innerHTML = "<input type='submit' value='Apply Filter'>";
}

const handleFilterSubmit = async (event) => {
  event.preventDefault();
  const nodeTrues = [];
  const relsTrues = [];
  const numNodes = (await getNodeNames()).length
  const numRels = (await getRelsNames()).length

  if(event.target) {
    
    for(let i=0;i<numNodes;i++) {
      if(event.target[i].checked) {
        nodeTrues.push(event.target[i].id)
      }
    }
  
    for(let i=numNodes;i<numRels+numNodes;i++) {
      if(event.target[i].checked) {
        relsTrues.push(event.target[i].id)
      }
    }
  }

  if(event.detail) {
    if(event.detail.checked) {
      nodeTrues.push(event.detail.id)
    }
  
    if(event.detail.checked) {
      relsTrues.push(event.detail.id)
    }
  }
  

  if(nodeTrues.length > relsTrues.length) {
    config.initial_cypher = "MATCH (p:"+nodeTrues[0]+") RETURN p"
    let graph = new NeoVis(config);
    graph.render();
  } else {
    config.initial_cypher = 'MATCH (p)-[r]->(m) WHERE type(r) = "'+relsTrues[0]+'" RETURN p,r,m'
    let graph = new NeoVis(config);
    graph.render();
  }
}

const searchHandle = async (event) => {
  event.preventDefault();
  if(event.target) {
    const search = event.target[0].value
    config.initial_cypher = 'MATCH (p)-[r]->(m) WHERE p.name =~ "'+search+'.*" OR m.title =~ "'+search+'.*" RETURN p,r,m'
    let graph = new NeoVis(config);
    graph.render();
  }
  if(event.detail) {
    const search = event.detail.value
    config.initial_cypher = 'MATCH (p)-[r]->(m) WHERE p.name =~ "'+search+'.*" OR m.title =~ "'+search+'.*" RETURN p,r,m'
    let graph = new NeoVis(config);
    graph.render();
  }
}

const pathBetweenHandle = async (event) => {
  event.preventDefault()
  
  const inputOne = event.target[0].value;
  const inputTwo = event.target[1].value;

  const shortestPathQuery = 'MATCH (z) WHERE z.name = "'+inputOne+'" OR z.title = "'+inputOne+'" MATCH (n) WHERE n.name = "'+inputTwo+'" OR n.title = "'+inputTwo+'" MATCH p=shortestPath((z)-[*]-(n)) WITH p WHERE length(p) > 1 RETURN p';
  config.initial_cypher = shortestPathQuery;
  let graph = new NeoVis(config);
  graph.render();
}

export { GraphCanvas, searchHandle, handleFilterSubmit, pathBetweenHandle }
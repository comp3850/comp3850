import * as d3 from 'd3'
import { countRels, countOfRelsPerNode } from "./DatabaseInfo"
import { searchHandle, handleFilterSubmit } from './GraphCanvas'

const TableCanvas = () => {
  makeBarGraphNodes()
  makeBarGraphRels()
  
  const content = (
    <>
      <div className={"div-container-TableCanvas"}>
        <p><strong>Relationship Types Counts</strong></p>
        <div id={'d3-container-rels'}></div>
        <p><strong>Node to Relationship Counts</strong></p>
        <div id={'d3-container-nodes'}></div>
      </div>
    </>
  )

  return content
}

const makeBarGraphRels = async () => {
  if(document.getElementById('d3-container-rels')) {
    document.getElementById('d3-container-rels').innerHTML = '';
  }
  const data = await countRels()
  const width = 1500;
  const height = 400;
  const margin = {top: 50, bottom: 50, left: 50, right: 50}

  const svg =  d3.select('#d3-container-rels')
    .append('svg')
    .attr('height', height - margin.top - margin.bottom)
    .attr('width', width - margin.left - margin.right)
    .attr('viewBox', [0,0,width,height]);

  const x = d3.scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1);

  const y = d3.scaleLinear()
    .domain([0,350000])
    .range([height - margin.bottom, margin.top]);

  svg
    .append('g')
    .attr('fill', 'royalblue')
    .selectAll('rect')
    .data(data.sort((a,b) => d3.descending(a.count, b.count)))
    .join('rect')
      .attr('x', (d,i) => x(i))
      .attr('y', d => y(d.count))
      .attr('height', d => y(0) - y(d.count))
      .attr('width', x.bandwidth())
      .attr("class", function(d) {
        return d.rel.replace(' ', '').concat(' relsCountRects');;
      })

  function xAxis(g) {
    g.attr('transform', 'translate(0,'+(height - margin.bottom)+')')
    .call(d3.axisBottom(x).tickFormat(i => data[i].rel))
    .attr('font-size', '1em')
  }

  function yAxis(g) {
    g.attr('transform', 'translate('+margin.left+', 0)')
    .call(d3.axisLeft(y).ticks(null, data.format))
    .attr('font-size', '1em')
  }

  svg.append('g').call(yAxis);
  svg.append('g').call(xAxis);
  svg.node();

  handleRelBarGraphClick();
}

const makeBarGraphNodes = async () => {
  if(document.getElementById('d3-container-rels')) {
    document.getElementById('d3-container-nodes').innerHTML = '';
  }
  const data = await countOfRelsPerNode()
  const width = 1500;
  const height = 400;
  const margin = {top: 50, bottom: 50, left: 50, right: 50}

  const svg =  d3.select('#d3-container-nodes')
    .append('svg')
    .attr('height', height - margin.top - margin.bottom)
    .attr('width', width - margin.left - margin.right)
    .attr('viewBox', [0,0,width,height]);

  const x = d3.scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1);

  const y = d3.scaleLinear()
    .domain([0,1000])
    .range([height - margin.bottom, margin.top]);

  svg
    .append('g')
    .attr('fill', 'royalblue')
    .selectAll('rect')
    .data(data.sort((a,b) => d3.descending(a.count, b.count)))
    .join('rect')
      .attr('x', (d,i) => x(i))
      .attr('y', d => y(d.count))
      .attr('height', d => y(0) - y(d.count))
      .attr('width', x.bandwidth())
      .attr("class", function(d) {
        return d.node.replace(' ', '').concat(' nodeCountRects');
      })

  function xAxis(g) {
    g.attr('transform', 'translate(0,'+(height - margin.bottom)+')')
    .call(d3.axisBottom(x).tickFormat(i => data[i].node))
    .attr('font-size', '1em')
  }

  function yAxis(g) {
    g.attr('transform', 'translate('+margin.left+', 0)')
    .call(d3.axisLeft(y).ticks(null, data.format))
    .attr('font-size', '1em')
  }

  svg.append('g').call(yAxis);
  svg.append('g').call(xAxis);
  svg.node();

  handleNodeBarGraphClick();
}

const handleNodeBarGraphClick = () => {
  const rectElements = document.getElementsByClassName('nodeCountRects');
  for(let i=0;i<rectElements.length;i++) {
    rectElements[i].addEventListener("click", function() {
      const cusEvent = new CustomEvent('nodeEventParcel', {
        detail: {
          value: rectElements[i].__data__.node
        }
      })
      window.scrollTo({top: 250, left: 0, behavior: 'smooth'});
      searchHandle(cusEvent);
    })
  }
}

const handleRelBarGraphClick = () => {
  const rectElements = document.getElementsByClassName('relsCountRects');
  for(let i=0;i<rectElements.length;i++) {
    rectElements[i].addEventListener("click", function() {
      const cusEvent = new CustomEvent('relEventParcel', {
        detail: {
          checked: true,
          id: rectElements[i].__data__.rel
        }
      })
      window.scrollTo({top: 250, left: 0, behavior: 'smooth'});
      handleFilterSubmit(cusEvent);
    })
  }
}

export { TableCanvas }
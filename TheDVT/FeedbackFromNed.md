28/4: (From meeting and possibly messenger (ie. last dot seems to be from there))
- top 10 nodes with most types of connections

- select two nodes, extra info comes up about them (number of nodes between them, most common type of nodes between them, etc.)
- Option to use a different way to visualise certain entities/relations (ie. bar graph)
- Filter nodes (or Summary nodes (ie. Nodes called People, IPs to reduce clutter) for massive datasets (remade in 17/5's section)
- Yeah I think direct buttons to check/uncheck the type of nodes that are displayed is really useful



17/5: (From meeting)
- Feature to see focus on things between and around two nodes.
    ie. When used:
       A set of buttons to choose how many relationships to remove/to keep on screen?
       Example?:
        Scenario with 2 levels of relation to keep on screen:
            Choosing Emily and Keenu
            - All unconnected nodes/relationships will be removed from screen.
            - The Matrix will be kept on screen.
                - All other actors from the Matrix could be kept on screen (first level relation)
                    - The other movies that those actors acted in will be kept on screen (second level relation)
                        - Other actors from these other moveies will NOT be on screen (third level relation)
            ((Or I'm making a mistake and The Matrix is the first level relation and everything else moves up one level?))
- Mentioned again:
    Ways to minimise graphics on huge dataset - ie. Using Filters
- Showing off graphs would also be nice. -Ned (paraphrased)